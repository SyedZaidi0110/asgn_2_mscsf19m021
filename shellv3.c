/*
*  Video Lecture: 22
*  Programmer: Arif Butt
*  Course: System Programming with Linux
*  Student: MSCSF19M021  Mohsin Raza 
*  shellv3.c: 
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#define MAX_LEN 512
#define MAXARGS 10
#define ARGLEN 30
#define PROMPT "cs525mscsf19m021@shell:"

int execute(char* arglist[]);
char** tokenize(char* cmdline);
char* read_cmd(char*, FILE*);
void Multiple_Cmds(char * cmdline, char redirection[3][512]);
int No_multipleCmds=0;
////////fuction for spliting the strings into substring///////
void substring(char strng[],int pos,int len,char Subs[])
{
 int index = 0;
    while(index<len){
         Subs[index] = strng[pos+index];
 	 index++;
    }
    Subs[strlen(Subs)]='#';
}
//////////////////////////////////////////////////////////////
int main(){
   char *cmdline;
   char *Buffer;
   char** arglist;
   long  Buffer_size = pathconf(".",_PC_PATH_MAX);
   char path[Buffer_size];
   Buffer = (char *)malloc((size_t)Buffer_size);
   char  prompt [Buffer_size+50]; 
   strcpy(prompt,PROMPT); 
/////////
   getcwd(Buffer,Buffer_size);
  // printf("%s\n", Buffer);
////for excluding the /root /////////////
   substring(Buffer,5,strlen(Buffer),path);
 //  printf("%s\n", path);
   strcat(prompt,path); 
 
   while((cmdline = read_cmd(prompt,stdin)) != NULL){
      if((arglist = tokenize(cmdline)) != NULL){
            execute(arglist);
       //  need to free arglist
         for(int j=0; j < MAXARGS+1; j++)
	         free(arglist[j]);
         free(arglist);
         free(cmdline);
      }
  }//end of while loop
   printf("\n");
   return 0;
}
int execute(char* arglist[]){
   int status;
   int cpid = fork();
   switch(cpid){
      case -1:
         perror("fork failed");
	      exit(1);
      case 0:
	      execvp(arglist[0], arglist);
 	      perror("Command not found...");
	      exit(1);
      default:
	      waitpid(cpid, &status, 0);
         printf("child exited with status %d \n", status >> 8);
         return 0;
   }
}
char** tokenize(char* cmdline){
//allocate memory
   char** arglist = (char**)malloc(sizeof(char*)* (MAXARGS+1));
   for(int j=0; j < MAXARGS+1; j++){
	   arglist[j] = (char*)malloc(sizeof(char)* ARGLEN);
      bzero(arglist[j],ARGLEN);
    }
   if(cmdline[0] == '\0')//if user has entered nothing and pressed enter key
      return NULL;
   int argnum = 0; //slots used
   char*cp = cmdline; // pos in string
   char*start;
   int len;
   while(*cp != '\0'){
      while(*cp == ' ' || *cp == '\t') //skip leading spaces
          cp++;
      start = cp; //start of the word
      len = 1;

      //find the end of the word
      while(*++cp != '\0' && !(*cp ==' ' || *cp == '\t'))
         len++;
      strncpy(arglist[argnum], start, len);
      arglist[argnum][len] = '\0';
      argnum++;
   }
   arglist[argnum] = NULL;
   return arglist;
}      

char* read_cmd(char* prompt, FILE* fp){
   printf("%s", prompt);
  int c; //input character
   int pos = 0; //position of character in cmdline
   char* cmdline = (char*) malloc(sizeof(char)*MAX_LEN);
   while((c = getc(fp)) != EOF){
       if(c == '\n')
	  break;
       cmdline[pos++] = c;
   }
//these two lines are added, in case user press ctrl+d to exit the shell
   if(c == EOF && pos == 0) 
      return NULL;
   cmdline[pos] = '\0';
   return cmdline;
}
void Multiple_Cmds(char * cmdline, char redirection[3][512])
{
	char ** cmds= getCommand(cmdline, ";");
	char ** args;

	int i=0;
	while (cmds[i]!=NULL)
	{
		args= tokenize(cmds[i], redirection);
		int j=0;
		execute (args, redirection);
		i++;
	}

}